## Development server

Run `npm start` or `npm run dev` for a dev server. Navigate to `http://localhost:9000/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run prod` to build the project. The build artifacts will be stored in the `dist/` directory. 



