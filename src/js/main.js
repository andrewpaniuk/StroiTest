import 'bootstrap/scss/bootstrap-grid.scss';
import $ from 'jquery';

$(document).ready(() => {

    $('.burger__button').on('click', function() {
        $(this).toggleClass('active');
        $('.navigation__list').slideToggle('400');
    });

})

