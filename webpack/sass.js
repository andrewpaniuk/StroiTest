module.exports = function() {
    return {
        module: {
            rules: [{
                test: /\.(sass|scss)$/,
                use: [{
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            }]
        }
    };
};